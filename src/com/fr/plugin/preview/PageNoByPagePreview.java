package com.fr.plugin.preview;

import com.fr.design.mainframe.JTemplate;
import com.fr.design.preview.PagePreview;

import java.util.HashMap;
import java.util.Map;

/**
 * @author richie
 * @date 2015-03-19
 * @since 8.0
 */
public class PageNoByPagePreview extends PagePreview {
    @Override
    public String nameForPopupItem() {
        return "����ҳԤ��";
    }

    @Override
    public String iconPathForPopupItem() {
        return "com/fr/design/images/buttonicon/pages.png";
    }

    @Override
    public String iconPathForLarge() {
        return "com/fr/design/images/buttonicon/pageb24.png";
    }

    @Override
    public void onClick(JTemplate<?, ?> jt) {
        jt.previewMenuActionPerformed(this);
    }

    @Override
    public int previewTypeCode() {
        return 10;
    }

    @Override
    public Map<String, Object> parametersForPreview() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("__bypagesize__", "false");
        return map;
    }
}
